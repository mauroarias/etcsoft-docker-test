package org.etcsoft.dockertest.docker;

import com.spotify.docker.client.DockerClient;
import com.spotify.docker.client.LogStream;
import com.spotify.docker.client.messages.ContainerConfig;
import com.spotify.docker.client.messages.ContainerCreation;
import com.spotify.docker.client.messages.Network;
import lombok.SneakyThrows;
import org.etcsoft.dockertest.DockerConstants;
import org.etcsoft.mysql.connector.repository.MysqlDataSource;
import org.etcsoft.tools.exception.EtcsoftException;
import org.junit.Before;
import org.junit.Test;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.etcsoft.mysql.connector.exception.MysqlErrorCodes.DATABASE_SCHEMA_ERROR;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MysqlDockerTest {

    private final static String IMAGE_NAME = "image";
    private final static String IMAGE_TAG = "tag";
    private final static String NETWORK_NAME = "network";
    private final static String CONTAINER_ID = "container";
    private final static String HOSTNAME = "hostname";
    private final static int WAITING_CONTAINER_LOOP_TIME = 1;

    private final DockerClient dockerClient = mock(DockerClient.class);
    private final MysqlDataSource mysqlDataSource = mock(MysqlDataSource.class);
    private final ContainerCreation containerCreation = mock(ContainerCreation.class);
    private final DataSource dataSource = mock(DataSource.class);
    private final Network network = mock(Network.class);
    private final LogStream logStream = mock(LogStream.class);
    private final Connection connection = mock(Connection.class);
    private final PreparedStatement preparedStatement = mock(PreparedStatement.class);

    @Before
    @SneakyThrows
    public void setMock() {
        when(dockerClient.createContainer(any(ContainerConfig.class))).thenReturn(containerCreation);
        when(containerCreation.id()).thenReturn(CONTAINER_ID);
        when(dockerClient.listNetworks()).thenReturn(Arrays.asList(network));
        when(network.name()).thenReturn(NETWORK_NAME);
        when(dockerClient.logs(eq(CONTAINER_ID),
                               any(DockerClient.LogsParam.class),
                               any(DockerClient.LogsParam.class))).thenReturn(logStream);
        when(logStream.readFully()).thenReturn(DockerConstants.DOCKER_MYSQL_WAITING_START_MESSAGE);
    }

    @Test
    public void whenInstanceAllArgOK_thenInstanceOk() {
        MysqlDocker docker = getInstanceArgOk();
		assertThat(docker.getContainerId()).isEqualTo(CONTAINER_ID);
    }

    @Test
    @SneakyThrows
    public void whenInstanceClose_ThenInstanceOk() {
        MysqlDocker docker = getInstanceArgOk();
        assertThat(docker.getContainerId()).isEqualTo(CONTAINER_ID);
        assertThat(docker.isClosed()).isEqualTo(false);
        docker.close();
        assertThat(docker.isClosed()).isEqualTo(true);
    }

    @Test
    public void whenGetDataSource_thenDataSourceOk() {
        MysqlDocker docker = getInstanceArgOk();
        assertThat(docker.getContainerId()).isEqualTo(CONTAINER_ID);
        assertThat(docker.getMysqlDataSource()).isEqualTo(mysqlDataSource);
    }

    @Test
    @SneakyThrows
    public void whenLoadSchema_thenOk() {
		when(mysqlDataSource.getDataSource()).thenReturn(dataSource);
        when(dataSource.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(any(String.class))).thenReturn(preparedStatement);
        MysqlDocker docker = getInstanceArgOk();
        assertThat(docker.getContainerId()).isEqualTo(CONTAINER_ID);
        docker.loadSchema(Arrays.asList("a", "b", "c"));
    }

    @Test
    @SneakyThrows
    public void whenLoadSchemaPreparedStatementException_ThenException() {
		when(mysqlDataSource.getDataSource()).thenReturn(dataSource);
		when(dataSource.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(any(String.class))).thenThrow(new SQLException("load schema exception"));
        MysqlDocker docker = getInstanceArgOk();
        assertThat(docker.getContainerId()).isEqualTo(CONTAINER_ID);
        assertThatThrownBy(() -> docker.loadSchema(Arrays.asList("a", "b", "c")))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("Error loading schema, exception: load schema exception!!!")
				.hasFieldOrPropertyWithValue("errorCode", DATABASE_SCHEMA_ERROR)
				.hasCauseInstanceOf(SQLException.class);
    }

    @Test
    @SneakyThrows
    public void whenLoadSchemaGetConnectionException_ThenException() {
		when(mysqlDataSource.getDataSource()).thenReturn(dataSource);
		when(dataSource.getConnection()).thenThrow(new SQLException("load schema exception"));
        MysqlDocker docker = getInstanceArgOk();
        assertThat(docker.getContainerId()).isEqualTo(CONTAINER_ID);

		assertThatThrownBy(() -> docker.loadSchema(Arrays.asList("a", "b", "c")))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("Error loading schema, exception: load schema exception!!!")
				.hasFieldOrPropertyWithValue("errorCode", DATABASE_SCHEMA_ERROR)
				.hasCauseInstanceOf(SQLException.class);
    }

    private MysqlDocker getInstanceArgOk() {
        return new MysqlDefaultDocker(IMAGE_NAME,
                                      IMAGE_TAG,
                                      HOSTNAME,
                                      WAITING_CONTAINER_LOOP_TIME,
                                      true,
                                      dockerClient,
                                      mysqlDataSource,
                                      null,
                                      null);
    }
}
