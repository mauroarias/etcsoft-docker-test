package org.etcsoft.dockertest.docker.it;

import com.mysql.jdbc.exceptions.jdbc4.MySQLSyntaxErrorException;
import lombok.SneakyThrows;
import org.etcsoft.dockertest.builders.DockerContainerFactory;
import org.etcsoft.dockertest.docker.EsDocker;
import org.etcsoft.elasticsearch.connector.repository.EsClient;
import org.etcsoft.tools.exception.EtcsoftException;
import org.etcsoft.tools.jackson.factory.ObjectMapperFactory;
import org.etcsoft.tools.resource.ResourceLoader;
import org.etcsoft.tools.utc.JVMUtcTimeZoneSetter;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.concurrent.CompletionException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.Assertions.fail;
import static org.etcsoft.mysql.connector.exception.MysqlErrorCodes.DATABASE_ACCESS;

public class EsTestIt
{
	private static String INDEX_NAME = "my-index-test";
	private static String DOC_NAME = "profile";

    private static EsDocker esDocker;
    private static EsClient esClient;

    @BeforeClass
    public static void testSuiteSetup() {
		esDocker = DockerContainerFactory.getEsBuilder().build();
		esClient = esDocker.getEsClient();
	}

	@Before
	public void testSetup() {
		new JVMUtcTimeZoneSetter().setUtcTimeZoneAsDefault();
	}

    @AfterClass
    @SneakyThrows
    public static void cleanup() {
		if (esDocker != null) {
			esDocker.close();
		}
    }

    @Test
	public void whenCreateAndDropIndexes_thenOk() {
    	final String indexName = "create-drop-index-name-test";
		assertThat(esClient.isIndexExists(indexName)).isEqualTo(false);
		esClient.createIndex(indexName);
		assertThat(esClient.isIndexExists(indexName)).isEqualTo(true);
		esClient.dropIndex(indexName);
		assertThat(esClient.isIndexExists(indexName)).isEqualTo(false);
	}

	@Test
	public void whenAddAlias_thenOk() {
		final String indexName = "add-alias-index-name-test";
		final String alias = "add-alias-alias-test";
		assertThat(esClient.isIndexExists(indexName)).isEqualTo(false);
		esClient.createIndex(indexName);
		assertThat(esClient.isIndexExists(indexName)).isEqualTo(true);
		assertThat(esClient.isIndexExists(alias)).isEqualTo(false);
		esClient.addAlias(indexName, alias);
		assertThat(esClient.isIndexExists(alias)).isEqualTo(true);
	}
//
//
//
//	@Test
//	public void whenSelectSingleValueWithParam_thenOk() {
//    	final String query = "SELECT count(*) FROM my_schema.profile WHERE name = ?";
//    	final String param = "count param";
//		final Long expectedCount = esDocker.getEsClient().selectSingleValue(query, param) + 1;
//
//		esDocker.loadSchema(Arrays.asList("use my_schema;",
//										  "INSERT INTO profile VALUES (4,'count param','1966-05-12',0);"));
//
//		assertThat(esDocker.getEsClient().selectSingleValue(query, param)).isEqualTo(expectedCount);
//	}
//
//	@Test
//	public void whenSelectSingleValueWithWrongQuery_thenException() {
//		final String query = "SELECT count(*) FROM my_schema.profiles";
//		assertThatThrownBy(() -> esDocker.getEsClient().selectSingleValue(query))
//				.isInstanceOf(EtcsoftException.class)
//				.hasMessage("Error selecting query: 'SELECT count(*) FROM my_schema.profiles'")
//				.hasFieldOrPropertyWithValue("errorCode", DATABASE_ACCESS)
//				.hasCauseInstanceOf(MySQLSyntaxErrorException.class);
//	}
//
//	@Test
//	public void whenSelectSingleValueWithNotParam_thenException() {
//		final String query = "SELECT count(*) FROM my_schema.profile WHERE name = ?";
//		assertThatThrownBy(() -> esDocker.getEsClient().selectSingleValue(query))
//				.isInstanceOf(EtcsoftException.class)
//				.hasMessage("Error selecting query: 'SELECT count(*) FROM my_schema.profile WHERE name = ?'")
//				.hasFieldOrPropertyWithValue("errorCode", DATABASE_ACCESS)
//				.hasCauseInstanceOf(SQLException.class);
//	}
//
//	@Test
//	public void whenSelectQuery_thenOk() {
//		final String query = "SELECT * FROM my_schema.device";
//
//		assertThat(esDocker.getEsClient().selectQuery(query, Device.class))
//				.hasFieldOrPropertyWithValue("id", 1L)
//				.hasFieldOrPropertyWithValue("status", Device.Status.ON)
//				.hasFieldOrPropertyWithValue("serial", "00d069463aba")
//				.hasFieldOrPropertyWithValue("enabled", false);
//	}
//
//	@Test
//	public void whenSelectQueryWithParam_thenOk() {
//
//		esDocker.loadSchema(Arrays.asList("use my_schema;",
//										  "INSERT INTO profile VALUES (5,'query','1966-05-12',0);"));
//
//		final String query = "SELECT * FROM my_schema.profile WHERE name = ?";
//		final String param = "query";
//
//		assertThat(esDocker.getEsClient().selectQuery(query, Profile.class, param))
//				.hasFieldOrPropertyWithValue("id", 5L)
//				.hasFieldOrPropertyWithValue("name", "query")
//				.hasFieldOrPropertyWithValue("birthday", Date.valueOf("1966-05-12"))
//				.hasFieldOrPropertyWithValue("enabled", false);
//	}
//
//	@Test
//	public void whenSelectQueryWithWrongQuery_thenException() {
//		final String query = "SELECT * FROM my_schema.profiles";
//		assertThatThrownBy(() -> esDocker.getEsClient().selectQuery(query, Profile.class))
//				.isInstanceOf(EtcsoftException.class)
//				.hasMessage("Error selecting query: 'SELECT * FROM my_schema.profiles'")
//				.hasFieldOrPropertyWithValue("errorCode", DATABASE_ACCESS)
//				.hasCauseInstanceOf(MySQLSyntaxErrorException.class);
//	}
//
//	@Test
//	public void whenSelectQueryWithNotParam_thenException() {
//		final String query = "SELECT * FROM my_schema.profile WHERE name = ?";
//		assertThatThrownBy(() -> esDocker.getEsClient().selectQuery(query, Profile.class))
//				.isInstanceOf(EtcsoftException.class)
//				.hasMessage("Error selecting query: 'SELECT * FROM my_schema.profile WHERE name = ?'")
//				.hasFieldOrPropertyWithValue("errorCode", DATABASE_ACCESS)
//				.hasCauseInstanceOf(SQLException.class);
//	}
//
//	@Test
//	public void whenSelectQueryList_thenOk() {
//
//		final String queryCount = "SELECT COUNT(*) FROM my_schema.profile";
//		final String query = "SELECT * FROM my_schema.profile";
//		final int size = esDocker.getEsClient().selectSingleValue(queryCount).intValue();
//
//		Profile prof1 = Profile.builder().id(1L).name("Peter").birthday(Date.valueOf("1986-01-11")).enabled(true).build();
//		Profile prof2 = Profile.builder().id(2L).name("Jean").birthday(Date.valueOf("1986-01-11")).enabled(true).build();
//
//		assertThat(esDocker.getEsClient().selectQueryList(query, Profile.class))
//				.hasSize(size)
//				.contains(prof1)
//				.contains(prof2);
//	}
//
//	@Test
//	public void whenSelectQueryListWithParam_thenOk() {
//
//		final String queryCount = "SELECT COUNT(*) FROM my_schema.profile WHERE id < ?";
//		final String query = "SELECT * FROM my_schema.profile WHERE id < ?";
//		final int param = 3;
//		final int size = esDocker.getEsClient().selectSingleValue(queryCount, param).intValue();
//
//		Profile prof1 = Profile.builder().id(1L).name("Peter").birthday(Date.valueOf("1986-01-11")).enabled(true).build();
//		Profile prof2 = Profile.builder().id(2L).name("Jean").birthday(Date.valueOf("1986-01-11")).enabled(true).build();
//
//		assertThat(esDocker.getEsClient().selectQueryList(query, Profile.class, param))
//				.hasSize(size)
//				.contains(prof1)
//				.contains(prof2);
//	}
//
//	@Test
//	public void whenSelectQueryListWithWrongQuery_thenException() {
//		final String query = "SELECT * FROM my_schema.profiles";
//		assertThatThrownBy(() -> esDocker.getEsClient().selectQueryList(query, Profile.class))
//				.isInstanceOf(EtcsoftException.class)
//				.hasMessage("Error selecting query: 'SELECT * FROM my_schema.profiles'")
//				.hasFieldOrPropertyWithValue("errorCode", DATABASE_ACCESS)
//				.hasCauseInstanceOf(MySQLSyntaxErrorException.class);
//	}
//
//	@Test
//	public void whenSelectQueryListWithNotParam_thenException() {
//		final String query = "SELECT * FROM my_schema.profile WHERE name = ?";
//		assertThatThrownBy(() -> esDocker.getEsClient().selectQueryList(query, Profile.class))
//				.isInstanceOf(EtcsoftException.class)
//				.hasMessage("Error selecting query: 'SELECT * FROM my_schema.profile WHERE name = ?'")
//				.hasFieldOrPropertyWithValue("errorCode", DATABASE_ACCESS)
//				.hasCauseInstanceOf(SQLException.class);
//	}
//
//	@Test
//	public void whenExecuteQuery_thenOk() {
//		final String query = "INSERT INTO my_schema.profile VALUES (33,'Jean33','1986-01-11',1)";
//		final String select = "SELECT * FROM my_schema.profile WHERE id = ?";
//		esDocker.getEsClient().executeQuery((conn) -> {
//			PreparedStatement stmt = conn.prepareStatement(query);
//			stmt.execute();
//		});
//		assertThat(esDocker.getEsClient().selectQuery(select, Profile.class, 33))
//				.isEqualTo(Profile
//								   .builder()
//								   .id(33L)
//								   .name("Jean33")
//								   .birthday(Date.valueOf("1986-01-11"))
//								   .enabled(true)
//								   .build());
//	}
//
//	@Test
//	public void whenSetGetAutoCommit_thenOk() {
//
//		final boolean autoCommit = esDocker.getEsClient().isAutocommit();
//
//		esDocker.getEsClient().setAutocommit(!autoCommit);
//
//		assertThat(esDocker.getEsClient().isAutocommit())
//				.isEqualTo(!autoCommit);
//
//		esDocker.getEsClient().setAutocommit(autoCommit);
//
//		assertThat(esDocker.getEsClient().isAutocommit())
//				.isEqualTo(autoCommit);
//	}
//
//	@Test
//	public void whenGetDataSourceExecuteQuery_thenOk() {
//		final String insertQuery = "INSERT INTO profile VALUES (?,'execute','1966-05-12',?);";
//
//    	try {
//    		esDocker.getEsClient().executeQuery((connection) -> {
//				PreparedStatement stmt = connection.prepareStatement(insertQuery);
//				stmt.setLong(1,7L);
//				stmt.setBoolean(2,true);
//				stmt.execute();
//
//			});
//		} catch (Exception ex) {
//			fail("exception not expected");
//		}
//	}
//
//	@Test
//	public void whenGetDataSourceRollbackIfException_thenOk() {
//		try {
//			esDocker.getEsClient().executeQuery((connection) -> {
//				try {
//					insertProfile(connection, 8L);
//				} catch(Exception ex) {
//					fail("exception not expected");
//				}
//
//				insertProfile(connection, 8L);
//
//				fail("exception not expected");
//			});
//		} catch (Exception ex) {}
//
//		assertThat(
//				esDocker
//						.getEsClient()
//						.selectQuery("SELECT * FROM profile where id = ?", Profile.class, 8))
//				.isNull();
//	}
//
//	@Test
//	public void whenSelectSingleValueAsync_thenOk() {
//		final String query = "SELECT count(*) FROM my_schema.profile";
//		final Long expectedCount = esDocker.getEsClient().selectSingleValue(query) + 1;
//
//		esDocker.loadSchema(Arrays.asList("use my_schema;",
//										  "INSERT INTO profile VALUES (13,'Robert','1966-05-12',0);"));
//
//		assertThat(esDocker.getEsClient().selectSingleValueAsync(query).join()).isEqualTo(expectedCount);
//	}
//
//	@Test
//	public void whenSelectSingleValueAsyncWithParam_thenOk() {
//		final String query = "SELECT count(*) FROM my_schema.profile WHERE name = ?";
//		final String param = "count param";
//		final Long expectedCount = esDocker.getEsClient().selectSingleValue(query, param) + 1;
//
//		esDocker.loadSchema(Arrays.asList("use my_schema;",
//										  "INSERT INTO profile VALUES (14,'count param','1966-05-12',0);"));
//
//		assertThat(esDocker.getEsClient().selectSingleValueAsync(query, param).join())
//				.isEqualTo(expectedCount);
//	}
//
//	@Test
//	public void whenSelectSingleValueAsyncWithWrongQuery_thenException() {
//		final String query = "SELECT count(*) FROM my_schema.profiles";
//		assertThatThrownBy(() -> esDocker.getEsClient().selectSingleValueAsync(query).join())
//				.isInstanceOf(CompletionException.class)
//				.hasCauseInstanceOf(EtcsoftException.class);
//	}
//
//	@Test
//	public void whenSelectSingleValueAsyncWithNotParam_thenException() {
//		final String query = "SELECT count(*) FROM my_schema.profile WHERE name = ?";
//		assertThatThrownBy(() -> esDocker.getEsClient().selectSingleValueAsync(query).join())
//				.isInstanceOf(CompletionException.class)
//				.hasCauseInstanceOf(EtcsoftException.class);
//	}
//
//	@Test
//	public void whenSelectQueryAsync_thenOk() {
//		final String query = "SELECT * FROM my_schema.device";
//
//		assertThat(esDocker.getEsClient().selectQueryAsync(query, Device.class).join())
//				.hasFieldOrPropertyWithValue("id", 1L)
//				.hasFieldOrPropertyWithValue("status", Device.Status.ON)
//				.hasFieldOrPropertyWithValue("serial", "00d069463aba")
//				.hasFieldOrPropertyWithValue("enabled", false);
//	}
//
//	@Test
//	public void whenSelectQueryAsyncWithParam_thenOk() {
//
//		esDocker.loadSchema(Arrays.asList("use my_schema;",
//										  "INSERT INTO profile VALUES (15,'query15','1966-05-12',0);"));
//
//		final String query = "SELECT * FROM my_schema.profile WHERE name = ?";
//		final String param = "query15";
//
//		assertThat(esDocker.getEsClient().selectQueryAsync(query, Profile.class, param).join())
//				.hasFieldOrPropertyWithValue("id", 15L)
//				.hasFieldOrPropertyWithValue("name", "query15")
//				.hasFieldOrPropertyWithValue("birthday", Date.valueOf("1966-05-12"))
//				.hasFieldOrPropertyWithValue("enabled", false);
//	}
//
//	@Test
//	public void whenSelectQueryAsyncWithWrongQuery_thenException() {
//		final String query = "SELECT * FROM my_schema.profiles";
//		assertThatThrownBy(() -> esDocker.getEsClient().selectQueryAsync(query, Profile.class).join())
//				.isInstanceOf(CompletionException.class)
//				.hasCauseInstanceOf(EtcsoftException.class);
//	}
//
//	@Test
//	public void whenSelectQueryAsyncWithNotParam_thenException() {
//		final String query = "SELECT * FROM my_schema.profile WHERE name = ?";
//		assertThatThrownBy(() -> esDocker.getEsClient().selectQueryAsync(query, Profile.class).join())
//				.isInstanceOf(CompletionException.class)
//				.hasCauseInstanceOf(EtcsoftException.class);
//	}
//
//	@Test
//	public void whenSelectQueryListAsync_thenOk() {
//
//		final String queryCount = "SELECT COUNT(*) FROM my_schema.profile";
//		final String query = "SELECT * FROM my_schema.profile";
//		final int size = esDocker.getEsClient().selectSingleValue(queryCount).intValue();
//
//		Profile prof1 = Profile.builder().id(1L).name("Peter").birthday(Date.valueOf("1986-01-11")).enabled(true).build();
//		Profile prof2 = Profile.builder().id(2L).name("Jean").birthday(Date.valueOf("1986-01-11")).enabled(true).build();
//
//		assertThat(esDocker.getEsClient().selectQueryListAsync(query, Profile.class).join())
//				.hasSize(size)
//				.contains(prof1)
//				.contains(prof2);
//	}
//
//	@Test
//	public void whenSelectQueryListAsyncWithParam_thenOk() {
//
//		final String queryCount = "SELECT COUNT(*) FROM my_schema.profile WHERE id < ?";
//		final String query = "SELECT * FROM my_schema.profile WHERE id < ?";
//		final int param = 3;
//		final int size = esDocker.getEsClient().selectSingleValue(queryCount, param).intValue();
//
//		Profile prof1 = Profile.builder().id(1L).name("Peter").birthday(Date.valueOf("1986-01-11")).enabled(true).build();
//		Profile prof2 = Profile.builder().id(2L).name("Jean").birthday(Date.valueOf("1986-01-11")).enabled(true).build();
//
//		assertThat(esDocker.getEsClient().selectQueryListAsync(query, Profile.class, param).join())
//				.hasSize(size)
//				.contains(prof1)
//				.contains(prof2);
//	}
//
//	@Test
//	public void whenSelectQueryListAsyncWithWrongQuery_thenException() {
//		final String query = "SELECT * FROM my_schema.profiles";
//		assertThatThrownBy(() -> esDocker.getEsClient().selectQueryListAsync(query, Profile.class).join())
//				.isInstanceOf(CompletionException.class)
//				.hasCauseInstanceOf(EtcsoftException.class);
//	}
//
//	@Test
//	public void whenSelectQueryListAsyncWithNotParam_thenException() {
//		final String query = "SELECT * FROM my_schema.profile WHERE name = ?";
//		assertThatThrownBy(() -> esDocker.getEsClient().selectQueryListAsync(query, Profile.class).join())
//				.isInstanceOf(CompletionException.class)
//				.hasCauseInstanceOf(EtcsoftException.class);
//	}
//
//	@Test
//	public void whenExecuteQueryAsync_thenOk() {
//		final String query = "INSERT INTO my_schema.profile VALUES (133,'Jean33','1986-01-11',1)";
//		final String select = "SELECT * FROM my_schema.profile WHERE id = ?";
//		esDocker.getEsClient().executeQueryAsync((conn) -> {
//			PreparedStatement stmt = conn.prepareStatement(query);
//			stmt.execute();
//		}).join();
//		assertThat(esDocker.getEsClient().selectQueryAsync(select, Profile.class, 133).join())
//				.isEqualTo(Profile
//								   .builder()
//								   .id(133L)
//								   .name("Jean33")
//								   .birthday(Date.valueOf("1986-01-11"))
//								   .enabled(true)
//								   .build());
//	}
//
//	@Test
//	public void whenGetDataSourceExecuteQueryAsync_thenOk() {
//		final String insertQuery = "INSERT INTO profile VALUES (?,'execute','1966-05-12',?);";
//
//		try {
//			esDocker.getEsClient().executeQueryAsync((connection) -> {
//				PreparedStatement stmt = connection.prepareStatement(insertQuery);
//				stmt.setLong(1,17L);
//				stmt.setBoolean(2,true);
//				stmt.execute();
//
//			}).join();
//		} catch (Exception ex) {
//			fail("exception not expected");
//		}
//	}
//
//	@Test
//	public void whenGetDataSourceRollbackIfExceptionAsync_thenOk() {
//		try {
//			esDocker.getEsClient().executeQueryAsync((connection) -> {
//				try {
//					insertProfile(connection, 18L);
//				} catch(Exception ex) {
//					fail("exception not expected");
//				}
//
//				insertProfile(connection, 18L);
//
//				fail("exception not expected");
//			}).join();
//		} catch (Exception ex) {}
//
//		assertThat(
//				esDocker
//						.getEsClient()
//						.selectQueryAsync("SELECT * FROM profile where id = ?", Profile.class, 18).join())
//				.isNull();
//	}
//
//	private void insertProfile(final Connection connection, final Long id) throws SQLException {
//		final String insertQuery = "INSERT INTO profile VALUES (?,'execute','1966-05-12',?);";
//		PreparedStatement stmt = connection.prepareStatement(insertQuery);
//		stmt.setLong(1, id);
//		stmt.setBoolean(2,true);
//		stmt.execute();
//	}
}
