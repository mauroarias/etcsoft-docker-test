package org.etcsoft.dockertest.docker;

import com.spotify.docker.client.DockerClient;
import com.spotify.docker.client.LogStream;
import com.spotify.docker.client.exceptions.DockerException;
import com.spotify.docker.client.messages.ContainerConfig;
import com.spotify.docker.client.messages.ContainerCreation;
import lombok.SneakyThrows;
import org.etcsoft.tools.exception.EtcsoftException;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.etcsoft.dockertest.DockerConstants.DOCKER_SPRING_STARTING_FAIL;
import static org.etcsoft.dockertest.exception.DockerErrorCodes.DOCKER_CLIENT_ERROR;
import static org.etcsoft.dockertest.exception.DockerErrorCodes.DOCKER_INTERRUPTED;
import static org.etcsoft.dockertest.exception.DockerErrorCodes.DOCKER_STARTING_ERROR;
import static org.etcsoft.dockertest.exception.DockerErrorCodes.DOCKER_STARTING_TIMEOUT;
import static org.etcsoft.tools.exception.UtilsErrorCode.INPUT_ERROR;
import static org.etcsoft.tools.exception.UtilsErrorCode.UNKNOWN_ERROR;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DockerTest {

    private final DockerClient dockerClient = mock(DockerClient.class);
    private final ContainerCreation containerCreation = mock(ContainerCreation.class);
    private final LogStream logStream = mock(LogStream.class);

    @Test
    public void whenInstanceAllArgOK_thenInstanceOk() {
        whenMockDefault(AbstractDocker.getWAITING_STARTING_CONTAINER_MESSAGE());
        final AbstractDocker docker = AbstractDocker.builderDefault(dockerClient);
		assertThat(docker.getContainerId()).isEqualTo(AbstractDocker.getCONTAINER_ID());
		assertThat(docker.getImageName()).isEqualTo(AbstractDocker.getIMAGE_FULL_NAME());
        assertThat(docker.getEnvironmentVars()).hasSize(2);
		assertThat(docker.getContainerLinks()).hasSize(1);
		assertThat(docker.getImageName()).isEqualTo(AbstractDocker.getIMAGE_FULL_NAME());
		assertThat(docker.getParentContainers()).isEqualTo(AbstractDocker.getCONTAINERS());
    }

    @Test
    public void whenInstanceExposedPortsEmpty_thenOk() {
        whenMockDefault(AbstractDocker.getWAITING_STARTING_CONTAINER_MESSAGE());
		AbstractDocker docker = AbstractDocker.builder(dockerClient).emptyPorts().build();
        assertThat(docker.getContainerId()).isEqualTo(AbstractDocker.getCONTAINER_ID());
    }

	@Test
	public void whenInstanceExposedPortsNull_thenOk() {
		whenMockDefault(AbstractDocker.getWAITING_STARTING_CONTAINER_MESSAGE());
		AbstractDocker docker = AbstractDocker.builder(dockerClient).nullPorts().build();
		assertThat(docker.getContainerId()).isEqualTo(AbstractDocker.getCONTAINER_ID());
	}

	@Test
	public void whenInstanceExposedPortsOnBlank_thenOk() {
		whenMockDefault(AbstractDocker.getWAITING_STARTING_CONTAINER_MESSAGE());
		AbstractDocker docker = AbstractDocker.builder(dockerClient).withBlankPort().build();
		assertThat(docker.getExposedPorts()).hasSize(1);
		assertThat(docker.getExposedPorts()).containsExactly("3306");
		assertThat(docker.getContainerId()).isEqualTo(AbstractDocker.getCONTAINER_ID());
	}

	@Test
	public void whenInstanceExposedPortsOnNull_thenOk() {
		whenMockDefault(AbstractDocker.getWAITING_STARTING_CONTAINER_MESSAGE());
		AbstractDocker docker = AbstractDocker.builder(dockerClient).withNullPort().build();
		assertThat(docker.getExposedPorts()).hasSize(1);
		assertThat(docker.getExposedPorts()).containsExactly("3306");
		assertThat(docker.getContainerId()).isEqualTo(AbstractDocker.getCONTAINER_ID());
	}

	@Test
	public void whenInstanceExposedPortsWithInvalidPort_thenOk() {
		whenMockDefault(AbstractDocker.getWAITING_STARTING_CONTAINER_MESSAGE());
		AbstractDocker docker = AbstractDocker.builder(dockerClient).withInvalidPort().build();
		assertThat(docker.getExposedPorts()).hasSize(1);
		assertThat(docker.getExposedPorts()).containsExactly("3306");
		assertThat(docker.getContainerId()).isEqualTo(AbstractDocker.getCONTAINER_ID());
	}

	@Test
    public void whenInstanceHostnameBlank_thenOk() {
        whenMockDefault(AbstractDocker.getWAITING_STARTING_CONTAINER_MESSAGE());
		AbstractDocker docker = AbstractDocker.builder(dockerClient).withHostname("").build();
        assertThat(docker.getContainerId()).isEqualTo(AbstractDocker.getCONTAINER_ID());

		docker = AbstractDocker.builder(dockerClient).withHostname(null).build();
        assertThat(docker.getContainerId()).isEqualTo(AbstractDocker.getCONTAINER_ID());
    }

    @Test
    public void whenInstanceTagBlank_thenInstanceOk() {
        whenMockDefault(AbstractDocker.getWAITING_STARTING_CONTAINER_MESSAGE());
        AbstractDocker docker = AbstractDocker.builder(dockerClient).withImageTag("").build();
        assertThat(docker.getContainerId()).isEqualTo(AbstractDocker.getCONTAINER_ID());
        assertThat(docker.getImageName()).isEqualTo(AbstractDocker.getIMAGE_FULL_NAME_LATEST_TAG());

		docker = AbstractDocker.builder(dockerClient).withImageTag(null).build();
        assertThat(docker.getContainerId()).isEqualTo(AbstractDocker.getCONTAINER_ID());
        assertThat(docker.getImageName()).isEqualTo(AbstractDocker.getIMAGE_FULL_NAME_LATEST_TAG());
    }

    @Test
    public void whenInstanceEnvEmpty_thenInstanceOk() {
		whenMockDefault(AbstractDocker.getWAITING_STARTING_CONTAINER_MESSAGE());
		final AbstractDocker docker = AbstractDocker.builder(dockerClient).emptyEnvs().build();
		assertThat(docker.getContainerId()).isEqualTo(AbstractDocker.getCONTAINER_ID());
		assertThat(docker.getImageName()).isEqualTo(AbstractDocker.getIMAGE_FULL_NAME());
	}

	@Test
	public void whenInstanceEnvNull_thenInstanceOk() {
		whenMockDefault(AbstractDocker.getWAITING_STARTING_CONTAINER_MESSAGE());
		final AbstractDocker docker = AbstractDocker.builder(dockerClient).nullEnvs().build();
        assertThat(docker.getContainerId()).isEqualTo(AbstractDocker.getCONTAINER_ID());
        assertThat(docker.getImageName()).isEqualTo(AbstractDocker.getIMAGE_FULL_NAME());
    }

	@Test
	public void whenInstanceEnvBlank_thenInstanceOk() {
		whenMockDefault(AbstractDocker.getWAITING_STARTING_CONTAINER_MESSAGE());
		AbstractDocker docker = AbstractDocker.builder(dockerClient).withBlankEnv().build();
		assertThat(docker.getContainerId()).isEqualTo(AbstractDocker.getCONTAINER_ID());
		assertThat(docker.getImageName()).isEqualTo(AbstractDocker.getIMAGE_FULL_NAME());

		docker = AbstractDocker.builder(dockerClient).withNullEnv().build();
		assertThat(docker.getContainerId()).isEqualTo(AbstractDocker.getCONTAINER_ID());
		assertThat(docker.getImageName()).isEqualTo(AbstractDocker.getIMAGE_FULL_NAME());
	}

	@Test
	public void whenInstanceLinkEmpty_thenInstanceOk() {
		whenMockDefault(AbstractDocker.getWAITING_STARTING_CONTAINER_MESSAGE());
		final AbstractDocker docker = AbstractDocker.builder(dockerClient).emptyLinks().build();
		assertThat(docker.getContainerId()).isEqualTo(AbstractDocker.getCONTAINER_ID());
		assertThat(docker.getImageName()).isEqualTo(AbstractDocker.getIMAGE_FULL_NAME());
	}

	@Test
	public void whenInstanceLinkNull_thenInstanceOk() {
		whenMockDefault(AbstractDocker.getWAITING_STARTING_CONTAINER_MESSAGE());
		final AbstractDocker docker = AbstractDocker.builder(dockerClient).nullLinks().build();
		assertThat(docker.getContainerId()).isEqualTo(AbstractDocker.getCONTAINER_ID());
		assertThat(docker.getImageName()).isEqualTo(AbstractDocker.getIMAGE_FULL_NAME());
	}

	@Test
	public void whenInstanceLinkBlank_thenInstanceOk() {
		whenMockDefault(AbstractDocker.getWAITING_STARTING_CONTAINER_MESSAGE());
		AbstractDocker docker = AbstractDocker.builder(dockerClient).withBlankLink().build();
		assertThat(docker.getContainerId()).isEqualTo(AbstractDocker.getCONTAINER_ID());
		assertThat(docker.getImageName()).isEqualTo(AbstractDocker.getIMAGE_FULL_NAME());

		docker = AbstractDocker.builder(dockerClient).withNullLink().build();
		assertThat(docker.getContainerId()).isEqualTo(AbstractDocker.getCONTAINER_ID());
		assertThat(docker.getImageName()).isEqualTo(AbstractDocker.getIMAGE_FULL_NAME());
	}

	@Test
	public void whenParentContainerEmpty_thenInstanceOk() {
		whenMockDefault(AbstractDocker.getWAITING_STARTING_CONTAINER_MESSAGE());
		final AbstractDocker docker = AbstractDocker.builder(dockerClient).emptyParentContainer().build();
		assertThat(docker.getContainerId()).isEqualTo(AbstractDocker.getCONTAINER_ID());
		assertThat(docker.getImageName()).isEqualTo(AbstractDocker.getIMAGE_FULL_NAME());
	}

	@Test
	public void whenParentContainerNull_thenInstanceOk() {
		whenMockDefault(AbstractDocker.getWAITING_STARTING_CONTAINER_MESSAGE());
		final AbstractDocker docker = AbstractDocker.builder(dockerClient).nullParentContainer().build();
		assertThat(docker.getContainerId()).isEqualTo(AbstractDocker.getCONTAINER_ID());
		assertThat(docker.getImageName()).isEqualTo(AbstractDocker.getIMAGE_FULL_NAME());
	}

	@Test
	public void whenGetParentContainer_thenInstanceOk() {
		whenMockDefault(AbstractDocker.getWAITING_STARTING_CONTAINER_MESSAGE());
		final AbstractDocker docker = AbstractDocker.builder(dockerClient).build();
		assertThat(docker.getParentContainers()).isNotSameAs(AbstractDocker.getCONTAINERS());
	}

	@Test
	public void whenInstanceImageNameBlank_thenException() {
		assertThatThrownBy(() -> AbstractDocker.builder(dockerClient).withImageName("").build())
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("Image name cannot be blank")
				.hasFieldOrPropertyWithValue("errorCode", INPUT_ERROR)
				.hasNoCause();

		assertThatThrownBy(() -> AbstractDocker.builder(dockerClient).withImageName(null).build())
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("Image name cannot be blank")
				.hasFieldOrPropertyWithValue("errorCode", INPUT_ERROR)
				.hasNoCause();
	}

	@Test
	public void whenInstanceWaitingMessageBlank_thenException() {
		assertThatThrownBy(() -> AbstractDocker.builder(dockerClient).withWaitingMessage("").build())
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("Waiting message cannot be empty or null")
				.hasFieldOrPropertyWithValue("errorCode", INPUT_ERROR)
				.hasNoCause();

		assertThatThrownBy(() -> AbstractDocker.builder(dockerClient).withWaitingMessage(null).build())
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("Waiting message cannot be empty or null")
				.hasFieldOrPropertyWithValue("errorCode", INPUT_ERROR)
				.hasNoCause();
	}

	@Test
	public void whenInstanceDockerClientNull_thenException() {
		whenMockDefault(AbstractDocker.getWAITING_STARTING_CONTAINER_MESSAGE());
		assertThatThrownBy(() -> AbstractDocker.builder(null).build())
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("Docker client cannot be null")
				.hasFieldOrPropertyWithValue("errorCode", INPUT_ERROR)
				.hasNoCause();
	}

	@Test
	public void whenParentContainerValueNull_thenException() {
		assertThatThrownBy(() -> AbstractDocker.builder(dockerClient).nullValueParentContainer().build())
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("parent container map contain null values")
				.hasFieldOrPropertyWithValue("errorCode", INPUT_ERROR)
				.hasNoCause();
	}

	@Test
	public void whenParentContainerKeyEmpty_thenException() {
		assertThatThrownBy(() -> AbstractDocker.builder(dockerClient).blankKeyParentContainer().build())
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("parent container map contain blank keys")
				.hasFieldOrPropertyWithValue("errorCode", INPUT_ERROR)
				.hasNoCause();
	}

	@Test
	public void whenParentContainerKeyNull_thenException() {
		assertThatThrownBy(() -> AbstractDocker.builder(dockerClient).nullKeyParentContainer().build())
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("parent container map contain blank keys")
				.hasFieldOrPropertyWithValue("errorCode", INPUT_ERROR)
				.hasNoCause();
	}

	@Test
	public void whenInstanceAllArgOKNotAutoPull_thenInstanceOk() {
		whenMockDefault(AbstractDocker.getWAITING_STARTING_CONTAINER_MESSAGE());
		AbstractDocker docker = AbstractDocker.builder(dockerClient).withAutoPull(false).build();
		assertThat(docker.getContainerId()).isEqualTo(AbstractDocker.getCONTAINER_ID());
	}

	@Test
	@SneakyThrows
	public void whenInstanceAllArgOKAutoPullDockerException_thenException() {
		whenMockDefault(AbstractDocker.getWAITING_STARTING_CONTAINER_MESSAGE());
		doThrow(new DockerException("docker client exception!!!"))
				.when(dockerClient)
				.pull(any(String.class));

		assertThatThrownBy(() -> AbstractDocker.builder(dockerClient).build())
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("docker client exception!!!")
				.hasFieldOrPropertyWithValue("errorCode", DOCKER_CLIENT_ERROR)
				.hasCauseInstanceOf(DockerException.class);
	}

	@Test
	@SneakyThrows
	public void whenInstanceAllArgOKAutoPullInterruptedException_thenException() {
		whenMockDefault(AbstractDocker.getWAITING_STARTING_CONTAINER_MESSAGE());
		doThrow(new InterruptedException("interrupted Exception!!!"))
				.when(dockerClient)
				.pull(any(String.class));

		assertThatThrownBy(() -> AbstractDocker.builder(dockerClient).build())
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("interrupted Exception!!!")
				.hasFieldOrPropertyWithValue("errorCode", DOCKER_INTERRUPTED)
				.hasCauseInstanceOf(InterruptedException.class);
	}

	@Test
	@SneakyThrows
	public void whenInstanceCreationContainerDockerException_thenException() {
		doThrow(new DockerException("creating container error!!!"))
				.when(dockerClient)
				.createContainer(any(ContainerConfig.class));

		assertThatThrownBy(() -> AbstractDocker.builder(dockerClient).build())
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("creating container error!!!")
				.hasFieldOrPropertyWithValue("errorCode", DOCKER_CLIENT_ERROR)
				.hasCauseInstanceOf(DockerException.class);
	}

	@Test
	@SneakyThrows
	public void whenInstanceCreationContainerInterruptedException_thenException() {
		doThrow(new InterruptedException("creating container error!!!"))
				.when(dockerClient)
				.createContainer(any(ContainerConfig.class));

		assertThatThrownBy(() -> AbstractDocker.builder(dockerClient).build())
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("creating container error!!!")
				.hasFieldOrPropertyWithValue("errorCode", DOCKER_INTERRUPTED)
				.hasCauseInstanceOf(InterruptedException.class);
	}

	@Test
	@SneakyThrows
	public void whenInstanceCreationContainerNull_thenException() {
		assertThatThrownBy(() -> AbstractDocker.builder(dockerClient).build())
				.isInstanceOf(EtcsoftException.class)
				.hasCauseInstanceOf(NullPointerException.class)
				.hasFieldOrPropertyWithValue("errorCode", UNKNOWN_ERROR);
	}

	@Test
	@SneakyThrows
	public void whenInstanceStartContainerDockerException_thenException() {
		when(dockerClient.createContainer(any(ContainerConfig.class))).thenReturn(containerCreation);
		when(containerCreation.id()).thenReturn(AbstractDocker.getCONTAINER_ID());
		doThrow(new DockerException("starting container error!!!"))
				.when(dockerClient)
				.startContainer(any(String.class));

		assertThatThrownBy(() -> AbstractDocker.builder(dockerClient).build())
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("starting container error!!!")
				.hasFieldOrPropertyWithValue("errorCode", DOCKER_CLIENT_ERROR)
				.hasCauseInstanceOf(DockerException.class);
	}

	@Test
	@SneakyThrows
	public void whenInstanceStartContainerInterruptedException_thenException() {
		when(dockerClient.createContainer(any(ContainerConfig.class))).thenReturn(containerCreation);
		when(containerCreation.id()).thenReturn(AbstractDocker.getCONTAINER_ID());

		doThrow(new InterruptedException("starting container error!!!"))
				.when(dockerClient)
				.startContainer(any(String.class));

		assertThatThrownBy(() -> AbstractDocker.builder(dockerClient).build())
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("starting container error!!!")
				.hasFieldOrPropertyWithValue("errorCode", DOCKER_INTERRUPTED)
				.hasCauseInstanceOf(InterruptedException.class);
	}

	@Test
	@SneakyThrows
	public void whenInstanceContainerIdNull_thenException() {
		when(dockerClient.createContainer(any(ContainerConfig.class))).thenReturn(containerCreation);
		when(containerCreation.id()).thenReturn(null);

		assertThatThrownBy(() -> AbstractDocker.builder(dockerClient).build())
				.isInstanceOf(EtcsoftException.class)
				.hasCauseInstanceOf(NullPointerException.class)
				.hasFieldOrPropertyWithValue("errorCode", UNKNOWN_ERROR);
	}

	@Test
	@SneakyThrows
	public void whenGetLogsDockerException_thenException() {
		when(dockerClient.createContainer(any(ContainerConfig.class))).thenReturn(containerCreation);
		when(containerCreation.id()).thenReturn(AbstractDocker.getCONTAINER_ID());
		doThrow(new DockerException("get log container error!!!"))
				.when(dockerClient)
				.logs(any(String.class), any(DockerClient.LogsParam.class), any(DockerClient.LogsParam.class));

		assertThatThrownBy(() -> AbstractDocker.builder(dockerClient).build())
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("get log container error!!!")
				.hasFieldOrPropertyWithValue("errorCode", DOCKER_CLIENT_ERROR)
				.hasCauseInstanceOf(DockerException.class);
	}

	@Test
	@SneakyThrows
	public void whenGetLogInterruptedException_thenException() {
		when(dockerClient.createContainer(any(ContainerConfig.class))).thenReturn(containerCreation);
		when(containerCreation.id()).thenReturn(AbstractDocker.getCONTAINER_ID());

		doThrow(new InterruptedException("get log container error!!!"))
				.when(dockerClient)
				.logs(any(String.class), any(DockerClient.LogsParam.class), any(DockerClient.LogsParam.class));

		assertThatThrownBy(() -> AbstractDocker.builder(dockerClient).build())
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("get log container error!!!")
				.hasFieldOrPropertyWithValue("errorCode", DOCKER_INTERRUPTED)
				.hasCauseInstanceOf(InterruptedException.class);
	}

	@Test
	@SneakyThrows
	public void whenGetLogNull_thenException() {
		when(dockerClient.createContainer(any(ContainerConfig.class))).thenReturn(containerCreation);
		when(containerCreation.id()).thenReturn(null);

		assertThatThrownBy(() -> AbstractDocker.builder(dockerClient).build())
				.isInstanceOf(EtcsoftException.class)
				.hasCauseInstanceOf(NullPointerException.class)
				.hasFieldOrPropertyWithValue("errorCode", UNKNOWN_ERROR);
	}

	@Test
	@SneakyThrows
	public void whenRemoveContainerDockerException_thenException() {
		when(dockerClient.createContainer(any(ContainerConfig.class))).thenReturn(containerCreation);
		when(containerCreation.id()).thenReturn(AbstractDocker.getCONTAINER_ID());
		when(dockerClient.logs(eq(AbstractDocker.getCONTAINER_ID()),
							   any(DockerClient.LogsParam.class),
							   any(DockerClient.LogsParam.class))).thenReturn(logStream);
		when(logStream.readFully()).thenReturn(DOCKER_SPRING_STARTING_FAIL);

		doThrow(new DockerException("Removing container container error!!!"))
				.when(dockerClient)
				.removeContainer(any(String.class));

		assertThatThrownBy(() -> AbstractDocker.builder(dockerClient).build())
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("Removing container container error!!!")
				.hasFieldOrPropertyWithValue("errorCode", DOCKER_CLIENT_ERROR)
				.hasCauseInstanceOf(DockerException.class);
	}

	@Test
	@SneakyThrows
	public void whenRemoveContainerInterruptedException_thenException() {
		when(dockerClient.createContainer(any(ContainerConfig.class))).thenReturn(containerCreation);
		when(containerCreation.id()).thenReturn(AbstractDocker.getCONTAINER_ID());
		when(dockerClient.logs(eq(AbstractDocker.getCONTAINER_ID()),
							   any(DockerClient.LogsParam.class),
							   any(DockerClient.LogsParam.class))).thenReturn(logStream);
		when(logStream.readFully()).thenReturn(DOCKER_SPRING_STARTING_FAIL);

		doThrow(new InterruptedException("Removing container container error!!!"))
				.when(dockerClient)
				.removeContainer(any(String.class));

		assertThatThrownBy(() -> AbstractDocker.builder(dockerClient).build())
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("Removing container container error!!!")
				.hasFieldOrPropertyWithValue("errorCode", DOCKER_INTERRUPTED)
				.hasCauseInstanceOf(InterruptedException.class);
	}

	@Test
	@SneakyThrows
	public void whenFailStartingContainer_thenException() {
		when(dockerClient.createContainer(any(ContainerConfig.class))).thenReturn(containerCreation);
		when(containerCreation.id()).thenReturn(AbstractDocker.getCONTAINER_ID());
		when(dockerClient.logs(eq(AbstractDocker.getCONTAINER_ID()),
							   any(DockerClient.LogsParam.class),
							   any(DockerClient.LogsParam.class))).thenReturn(logStream);
		when(logStream.readFully()).thenReturn(DOCKER_SPRING_STARTING_FAIL);

		assertThatThrownBy(() -> AbstractDocker.builder(dockerClient).build())
				.isInstanceOf(EtcsoftException.class)
				.hasMessage(DOCKER_SPRING_STARTING_FAIL)
				.hasFieldOrPropertyWithValue("errorCode", DOCKER_STARTING_ERROR)
				.hasNoCause();
	}

	@Test
	@SneakyThrows
	public void whenStartingContainerTimeout_thenException() {
		when(dockerClient.createContainer(any(ContainerConfig.class))).thenReturn(containerCreation);
		when(containerCreation.id()).thenReturn(AbstractDocker.getCONTAINER_ID());
		when(dockerClient.logs(eq(AbstractDocker.getCONTAINER_ID()),
							   any(DockerClient.LogsParam.class),
							   any(DockerClient.LogsParam.class))).thenReturn(logStream);
		when(logStream.readFully()).thenReturn("");

		assertThatThrownBy(() -> AbstractDocker.builder(dockerClient).build())
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("Timeout expired during starting container!!!")
				.hasFieldOrPropertyWithValue("errorCode", DOCKER_STARTING_TIMEOUT)
				.hasNoCause();
	}

    @SneakyThrows
    private void whenMockDefault(final String logMessage) {
        when(dockerClient.createContainer(any(ContainerConfig.class))).thenReturn(containerCreation);
        when(containerCreation.id()).thenReturn(AbstractDocker.getCONTAINER_ID());
        when(dockerClient.logs(eq(AbstractDocker.getCONTAINER_ID()),
                any(DockerClient.LogsParam.class),
                any(DockerClient.LogsParam.class))).thenReturn(logStream);
        when(logStream.readFully()).thenReturn(logMessage);
    }
}
