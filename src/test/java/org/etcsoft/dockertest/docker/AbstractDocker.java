package org.etcsoft.dockertest.docker;

import com.spotify.docker.client.DockerClient;
import lombok.Getter;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static java.lang.String.format;
import static org.mockito.Mockito.mock;

public class AbstractDocker extends DockerAbstractContainer {

	private final static String IMAGE_NAME = "image";
	private final static String IMAGE_TAG = "tag";
	@Getter
	private final static String CONTAINER_ID = "container";
	@Getter
	private final static String IMAGE_FULL_NAME = format("%s:%s", IMAGE_NAME, IMAGE_TAG);
	@Getter
	private final static String IMAGE_FULL_NAME_LATEST_TAG = format("%s:latest", IMAGE_NAME);
	@Getter
	private final static String WAITING_STARTING_CONTAINER_MESSAGE = "It is ready!!!";
	@Getter
	private final static Map<String, DockerContainer> CONTAINERS = new HashMap<>();

	AbstractDocker(final String imageName,
				   final String imageTag,
				   final String hostname,
				   final String waitingMessage,
				   final int dockerWaitingLoop,
				   final boolean autoPull,
				   final DockerClient dockerClient,
				   final Set<String> envs,
				   final Set<String> exposedPorts,
				   final Set<String> links,
				   final Map<String, DockerContainer> parentContainers) {
        super(imageName,
			  imageTag,
			  hostname,
			  waitingMessage,
			  dockerWaitingLoop,
			  autoPull,
			  dockerClient,
			  envs,
			  exposedPorts,
			  links,
			  parentContainers);
    }

	static AbstractDockerBuilder builder(DockerClient dockerClient) {
		return new AbstractDockerBuilder(dockerClient);
    }

	static AbstractDocker builderDefault(DockerClient dockerClient) {
		return new AbstractDockerBuilder(dockerClient).build();
	}

	static class AbstractDockerBuilder {
		private final static String HOSTNAME = "hostname";
		private final static Set<String> PORT = new HashSet<>();
		private final static Set<String> PORTS = new HashSet<>();
		private final static Set<String> ENVS = new HashSet<>();
		private final static Set<String> LINKS = new HashSet<>();
		private final static Set<String> SET_WITH_NULL = new HashSet<>();
		private final static Set<String> SET_WITH_BLANK = new HashSet<>();
		private final static int WAITING_CONTAINER_LOOP_TIME = 1;

		static {
			PORT.add("3333");
			PORTS.add("3333");
			PORTS.add("6666");
			PORTS.add("4444");
			ENVS.add("A");
			ENVS.add("B");
			SET_WITH_NULL.add(null);
			SET_WITH_NULL.add("3306");
			SET_WITH_BLANK.add("");
			SET_WITH_BLANK.add("3306");
			LINKS.add("376986436");
			CONTAINERS.put("a", mock(DockerContainer.class));
		}

		private String imageName = IMAGE_NAME;
		private String imageTag = IMAGE_TAG;
		private String hostname = HOSTNAME;
		private String waitingMessage = WAITING_STARTING_CONTAINER_MESSAGE;
		private int dockerWaitingLoop = WAITING_CONTAINER_LOOP_TIME;
		private boolean autoPull = true;
		private final DockerClient dockerClient;
		private Set<String> envs = ENVS;
		private Set<String> exposedPorts = PORTS;
		private Set<String> links = LINKS;
		private Map<String, DockerContainer> parentContainers = CONTAINERS;

		AbstractDockerBuilder(DockerClient dockerClient) {
			this.dockerClient = dockerClient;
		}

		public AbstractDockerBuilder withImageName(String imageName) {
			this.imageName = imageName;
			return this;
		}

		public AbstractDockerBuilder withImageTag(String imageTag) {
			this.imageTag = imageTag;
			return this;
		}

		public AbstractDockerBuilder withHostname(String hostname) {
			this.hostname = hostname;
			return this;
		}

		public AbstractDockerBuilder withWaitingMessage(String waitingMessage) {
			this.waitingMessage = waitingMessage;
			return this;
		}

		public AbstractDockerBuilder withDockerWaitingLoop(int dockerWaitingLoop) {
			this.dockerWaitingLoop = dockerWaitingLoop;
			return this;
		}

		public AbstractDockerBuilder withAutoPull(boolean autoPull) {
			this.autoPull = autoPull;
			return this;
		}

		public AbstractDockerBuilder withEnvs(Set<String> envs) {
			this.envs = envs;
			return this;
		}

		public AbstractDockerBuilder withBlankEnv() {
			this.envs = SET_WITH_BLANK;
			return this;
		}

		public AbstractDockerBuilder withNullEnv() {
			this.envs = SET_WITH_NULL;
			return this;
		}

		public AbstractDockerBuilder emptyEnvs() {
			this.envs = new HashSet<>();
			return this;
		}

		public AbstractDockerBuilder nullEnvs() {
			this.envs = null;
			return this;
		}

		public AbstractDockerBuilder withExposedPorts(Set<String> exposedPorts) {
			this.exposedPorts = exposedPorts;
			return this;
		}

		public AbstractDockerBuilder withBlankPort() {
			this.exposedPorts = SET_WITH_BLANK;
			return this;
		}

		public AbstractDockerBuilder withNullPort() {
			this.exposedPorts = SET_WITH_NULL;
			return this;
		}

		public AbstractDockerBuilder withInvalidPort() {
			this.exposedPorts = new HashSet<>();
			this.exposedPorts.add("-5");
			this.exposedPorts.add("3306");
			return this;
		}

		public AbstractDockerBuilder emptyPorts() {
			this.exposedPorts = new HashSet<>();
			return this;
		}

		public AbstractDockerBuilder nullPorts() {
			this.exposedPorts = null;
			return this;
		}

		public AbstractDockerBuilder withLinks(Set<String> links) {
			this.links = links;
			return this;
		}

		public AbstractDockerBuilder withBlankLink() {
			this.links = SET_WITH_BLANK;
			return this;
		}

		public AbstractDockerBuilder withNullLink() {
			this.links = SET_WITH_NULL;
			return this;
		}

		public AbstractDockerBuilder emptyLinks() {
			this.links = new HashSet<>();
			return this;
		}

		public AbstractDockerBuilder nullLinks() {
			this.links = null;
			return this;
		}

		public AbstractDockerBuilder withParentContainer(Map<String, DockerContainer> parentContainer) {
			this.parentContainers = parentContainer;
			return this;
		}

		public AbstractDockerBuilder blankKeyParentContainer() {
			this.parentContainers = new HashMap<>();
			this.parentContainers.put("", mock(DockerContainer.class));
			return this;
		}

		public AbstractDockerBuilder nullKeyParentContainer() {
			this.parentContainers = new HashMap<>();
			this.parentContainers.put(null, mock(DockerContainer.class));
			return this;
		}

		public AbstractDockerBuilder nullValueParentContainer() {
			this.parentContainers = new HashMap<>();
			this.parentContainers.put("uuu", null);
			return this;
		}

		public AbstractDockerBuilder nullParentContainer() {
			this.parentContainers = null;
			return this;
		}

		public AbstractDockerBuilder emptyParentContainer() {
			this.parentContainers = new HashMap<>();
			return this;
		}

		public AbstractDocker build() {
			return new AbstractDocker(imageName,
									  imageTag,
									  hostname,
									  waitingMessage,
									  dockerWaitingLoop,
									  autoPull,
									  dockerClient,
									  envs,
									  exposedPorts,
									  links,
									  parentContainers);
		}
	}

	public Set<String> getEnvironmentVars() {
		return environmentsVars;
	}

	public Set<String> getContainerLinks() {
		return containerLinks;
	}
}
