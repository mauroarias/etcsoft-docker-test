CREATE DATABASE my_schema;

use my_schema;

DROP TABLE IF EXISTS `api_keys`;
DROP TABLE IF EXISTS `kiosk`;

CREATE TABLE `profile` (
  `id` bigint(20) unsigned NOT NULL,
  `name` varchar(40) NOT NULL,
  `birthday` date NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
);

CREATE TABLE `device` (
  `id` int(10) NOT NULL,
  `status` enum('ON','OFF') DEFAULT 'ON',
  `serial` varchar(100) DEFAULT NULL,
  `enabled` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
);

INSERT INTO `profile` VALUES (1,'Peter','1986-01-11',1);
INSERT INTO `profile` VALUES (2,'Jean','1986-01-11',1);

INSERT INTO `device` VALUES (1, 'ON', '00d069463aba', 0);
