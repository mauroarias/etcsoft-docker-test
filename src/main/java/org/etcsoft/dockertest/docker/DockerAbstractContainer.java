package org.etcsoft.dockertest.docker;

import com.spotify.docker.client.DockerClient;
import com.spotify.docker.client.LogStream;
import com.spotify.docker.client.exceptions.DockerException;
import com.spotify.docker.client.messages.ContainerConfig;
import com.spotify.docker.client.messages.HostConfig;
import com.spotify.docker.client.messages.PortBinding;
import lombok.Getter;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;
import org.etcsoft.tools.exception.EtcsoftException;
import org.etcsoft.tools.validation.Validator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static java.lang.String.format;
import static org.etcsoft.dockertest.DockerConstants.DOCKER_SPRING_STARTING_FAIL;
import static org.etcsoft.dockertest.DockerConstants.DOCKER_WAITING_LOOP_DURATION;
import static org.etcsoft.dockertest.exception.DockerErrorCodes.DOCKER_CLIENT_ERROR;
import static org.etcsoft.dockertest.exception.DockerErrorCodes.DOCKER_INTERRUPTED;
import static org.etcsoft.dockertest.exception.DockerErrorCodes.DOCKER_STARTING_ERROR;
import static org.etcsoft.dockertest.exception.DockerErrorCodes.DOCKER_STARTING_TIMEOUT;
import static org.etcsoft.tools.exception.UtilsErrorCode.INPUT_ERROR;
import static org.etcsoft.tools.exception.UtilsErrorCode.UNKNOWN_ERROR;

public abstract class DockerAbstractContainer implements AutoCloseable {
	private final String dockerWaitingMessage;
	private final String hostname;
	private final Map<String, DockerContainer> parentContainers = new HashMap<>();
	private final int dockerWaitingLoopTime;
	private final Validator validator = new Validator();

	protected final Set<String> exposedPorts = new HashSet<>();
	protected final DockerClient dockerClient;
	final Set<String> containerLinks = new HashSet<>();
	final Set<String> environmentsVars = new HashSet<>();

	@Getter
	protected final String imageName;
	@Getter
	protected String containerId;
	@Getter
	protected boolean isClosed = true;
	@Getter
	protected String containerName;

	protected DockerAbstractContainer(final String imageName,
									  final String imageTag,
									  final String hostname,
									  final String dockerWaitingMessage,
									  final int dockerWaitingLoopTime,
									  final boolean autoPull,
									  final DockerClient dockerClient,
									  final Set<String> environmentsVars,
									  final Set<String> exposedPorts,
									  final Set<String> containerLinks,
									  final Map<String, DockerContainer> parentContainers) {
		try {
			copyHashSet(environmentsVars, this.environmentsVars, true);
			copyHashSet(containerLinks, this.containerLinks, true);
			copyHashSet(exposedPorts, this.exposedPorts, false);

			//argument validator
			validator.throwIfInstanceBlank(imageName, "Image name cannot be blank", INPUT_ERROR);
			validator.throwIfInstanceBlank(dockerWaitingMessage, "Waiting message cannot be empty or null", INPUT_ERROR);
			validator.throwIfInstanceNull(dockerClient, "Docker client cannot be null", INPUT_ERROR);
			validator.throwIfMapContainBlankKey(parentContainers, "parent container map contain blank keys", INPUT_ERROR);
			validator.throwIfMapContainNullValue(parentContainers, "parent container map contain null values", INPUT_ERROR);
			validator.throwIfListContainNull(this.environmentsVars, "Enviroments vars", INPUT_ERROR);
			validator.throwIfListContainNull(this.containerLinks, "container links", INPUT_ERROR);
			validator.throwIfListContainNull(this.exposedPorts, "exposed ports", INPUT_ERROR);

			this.dockerWaitingMessage = dockerWaitingMessage;
			this.dockerWaitingLoopTime = dockerWaitingLoopTime;
			this.dockerClient = dockerClient;
			this.imageName = format("%s:%s", imageName, StringUtils.isBlank(imageTag) ? "latest" : imageTag);
			this.hostname = hostname;

			if(parentContainers != null) {
				this.parentContainers.putAll(parentContainers);
			}

			//pulling image
			if(autoPull) {
				dockerClient.pull(this.imageName);
			}

			//start and wait for container
			this.containerId = startAndWait4Image();
			isClosed = false;
		}
		catch(DockerException cause) {
			throw new EtcsoftException(DOCKER_CLIENT_ERROR, cause.getMessage(), cause);
		}
		catch(InterruptedException cause) {
			throw new EtcsoftException(DOCKER_INTERRUPTED, cause.getMessage(), cause);
		}
		catch(EtcsoftException ex) {
			throw ex;
		}
		catch(Exception cause) {
			throw new EtcsoftException(UNKNOWN_ERROR, cause.getMessage(), cause);
		}
	}

	public Set<String> getExposedPorts() {
		return new HashSet<>(this.exposedPorts);
	}

	public Map<String, DockerContainer> getParentContainers() {
		return new HashMap<>(this.parentContainers);
	}

	@Override
	@SneakyThrows
	public void close() throws Exception {
		for(DockerContainer container : this.parentContainers.values()) {
			container.close();
		}
		closeContainer();
	}

	protected String getRandomPort(final String port) {
		try {
			final PortBinding portBinding =
					dockerClient
							.inspectContainer(containerId)
							.networkSettings()
							.ports()
							.get(format("%s/tcp", port))
							.stream()
							.findFirst()
							.get();

			if(portBinding == null || StringUtils.isBlank(portBinding.hostPort())) {
				return null;
			}

			return portBinding.hostPort();
		}
		catch(DockerException cause) {
			throw new EtcsoftException(DOCKER_CLIENT_ERROR, cause.getMessage(), cause);
		}
		catch(InterruptedException cause) {
			throw new EtcsoftException(DOCKER_INTERRUPTED, cause.getMessage(), cause);
		}
		catch(EtcsoftException ex) {
			throw ex;
		}
		catch(Exception cause) {
			throw new EtcsoftException(UNKNOWN_ERROR, cause.getMessage(), cause);
		}
	}

	protected void closeContainer() {
		try {
			//Stopping container & closing docker client
			if(dockerClient != null) {
				if(!StringUtils.isBlank(containerId)) {
					dockerClient.stopContainer(containerId, 20);
					dockerClient.removeContainer(containerId);
				}
				dockerClient.close();
				isClosed = true;
			}
		}
		catch(DockerException cause) {
			throw new EtcsoftException(DOCKER_CLIENT_ERROR, cause.getMessage(), cause);
		}
		catch(InterruptedException cause) {
			throw new EtcsoftException(DOCKER_INTERRUPTED, cause.getMessage(), cause);
		}
		catch(EtcsoftException ex) {
			throw ex;
		}
		catch(Exception cause) {
			throw new EtcsoftException(UNKNOWN_ERROR, cause.getMessage(), cause);
		}
	}

	private void copyHashSet(final Set<String> source, Set<String> dest, final boolean isNotPort) {
		if(source != null) {
			if(dest == null) {
				dest = new HashSet<>();
			}
			dest.addAll(source
								.stream()
								.filter(item -> StringUtils.isNotBlank(item) &&
												( isNotPort ||
												  ( StringUtils.isNumeric(item) &&
													validator.isValidPortValue(Integer.valueOf(item)))))
								.collect(Collectors.toSet()));
		}
	}

	private Map<String, List<PortBinding>> buildPortBuilding() {
		final Map<String, List<PortBinding>> portBindings = new HashMap<>();
		if(exposedPorts != null) {
			for(String port : exposedPorts) {
				List<PortBinding> hostPorts = new ArrayList<>();
				hostPorts.add(PortBinding.randomPort("0.0.0.0"));
				portBindings.put(format("%s/tcp", port), hostPorts);
			}
		}
		return portBindings;
	}

	private HostConfig buildHostConfig() {
		return HostConfig
				.builder()
				.links(containerLinks.toArray(new String[containerLinks.size()]))
				.portBindings(buildPortBuilding())
				.publishAllPorts(true)
				.build();
	}

	private String startAndWait4Image() throws DockerException, InterruptedException, IOException {
		final String containerId = prepareCreateContainer();
		dockerClient.startContainer(containerId);
		if(waitContainerUp(containerId)) {
			return containerId;
		}
		throw new EtcsoftException(DOCKER_STARTING_TIMEOUT, "Timeout expired during starting container!!!");
	}

	private Set<String> buildExposedPorts() {
		Set<String> exposedPorts = new HashSet<>();
		if(this.exposedPorts != null) {
			exposedPorts = this.exposedPorts.stream().map(port -> format("%s/tcp", port)).collect(Collectors.toSet());
		}
		return exposedPorts;
	}

	private String prepareCreateContainer() throws DockerException, InterruptedException {
		//prepare container config
		final ContainerConfig containerConfig =
				ContainerConfig
						.builder()
						.hostConfig(buildHostConfig())
						.attachStdin(true)
						.hostname(hostname)
						.image(imageName)
						.env(environmentsVars.toArray(new String[environmentsVars.size()]))
						.exposedPorts(buildExposedPorts())
						.build();
		return dockerClient.createContainer(containerConfig).id();
	}

	private boolean waitContainerUp(final String containerId) throws DockerException, InterruptedException, IOException {
		//waiting for container
		int waitCounter = dockerWaitingLoopTime;
		while(waitCounter-- != 0) {
			final LogStream dockerLog = dockerClient.logs(containerId,
														  DockerClient.LogsParam.stderr(),
														  DockerClient.LogsParam.stdout());
			try {
				final String logStream = dockerLog.readFully();
				if(logStream.contains(dockerWaitingMessage)) {
					Thread.sleep(DOCKER_WAITING_LOOP_DURATION);
					return true;
				}
				if(logStream.contains(DOCKER_SPRING_STARTING_FAIL)) {
					dockerClient.removeContainer(containerId);
					throw new EtcsoftException(DOCKER_STARTING_ERROR, logStream);
				}
				Thread.sleep(DOCKER_WAITING_LOOP_DURATION);
			}
			finally {
				if(dockerLog != null) {
					dockerLog.close();
				}
			}
		}
		return false;
	}
}
