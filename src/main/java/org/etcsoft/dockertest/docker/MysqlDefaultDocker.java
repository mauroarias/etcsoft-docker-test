package org.etcsoft.dockertest.docker;

import com.spotify.docker.client.DefaultDockerClient;
import com.spotify.docker.client.DockerClient;
import com.zaxxer.hikari.HikariDataSource;
import lombok.SneakyThrows;
import org.etcsoft.mysql.connector.repository.HikariMysqlDataSource;
import org.etcsoft.mysql.connector.repository.MysqlDataSource;
import org.etcsoft.tools.jackson.factory.ObjectMapperFactory;
import org.etcsoft.tools.exception.EtcsoftException;

import java.sql.Connection;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static java.lang.String.format;
import static org.etcsoft.dockertest.DockerConstants.DOCKER_MYSQL_HOST;
import static org.etcsoft.dockertest.DockerConstants.DOCKER_MYSQL_PASSWD;
import static org.etcsoft.dockertest.DockerConstants.DOCKER_MYSQL_PORT;
import static org.etcsoft.dockertest.DockerConstants.DOCKER_MYSQL_USER;
import static org.etcsoft.dockertest.DockerConstants.DOCKER_MYSQL_WAITING_LOOP;
import static org.etcsoft.dockertest.DockerConstants.DOCKER_MYSQL_WAITING_START_MESSAGE;
import static org.etcsoft.dockertest.DockerConstants.DOCKER_URI;
import static org.etcsoft.mysql.connector.exception.MysqlErrorCodes.DATABASE_SCHEMA_ERROR;

public final class MysqlDefaultDocker extends DockerAbstractContainer implements MysqlDocker {
    private MysqlDataSource mysqlDataSource = null;

    //general constructor
    public MysqlDefaultDocker(final String imageName,
                              final String imageTag,
							  final String hostname,
                              final boolean autoPull,
                              final Set<String> containerLinks,
                              final Map<String, DockerContainer> parentContainers) {
        this(imageName,
			 imageTag,
			 hostname,
			 DOCKER_MYSQL_WAITING_LOOP,
			 autoPull,
			 null,
			 null,
			 containerLinks,
			 parentContainers);
    }

    //unit test constructor, package access only
    MysqlDefaultDocker(final String imageName,
					   final String imageTag,
					   final String hostname,
					   final int secWaitingLoop,
					   final boolean autoPull,
					   final DockerClient dockerClient,
					   final MysqlDataSource mysqlDataSource,
					   final Set<String> containerLinks,
					   final Map<String, DockerContainer> parentContainers) {
        super(imageName,
			  imageTag,
			  hostname,
			  DOCKER_MYSQL_WAITING_START_MESSAGE,
			  secWaitingLoop,
			  autoPull,
			  dockerClient == null ? new DefaultDockerClient(DOCKER_URI) : dockerClient,
			  new HashSet<>(Arrays.asList(format("MYSQL_ROOT_PASSWORD=%s", DOCKER_MYSQL_PASSWD),
										  "MYSQL_ROOT_HOST=%")),
              new HashSet<>(Arrays.asList(DOCKER_MYSQL_PORT)),
              containerLinks,
              parentContainers
        );
        if(mysqlDataSource != null) {
            this.mysqlDataSource = mysqlDataSource;
        } else {
            this.mysqlDataSource = getMysqlDataSource();
        }
    }

    @SneakyThrows
    public MysqlDataSource getMysqlDataSource() {
        if(mysqlDataSource == null) {
            //starting Hikari datasource
            final HikariDataSource connection = new HikariDataSource();
			connection.setDriverClassName("com.mysql.jdbc.Driver");
			connection.setJdbcUrl(format("jdbc:mysql://%s:%s",
											 DOCKER_MYSQL_HOST,
											 getRandomPort(DOCKER_MYSQL_PORT)));
			connection.setUsername(DOCKER_MYSQL_USER);
			connection.setPassword(DOCKER_MYSQL_PASSWD);
			connection.setAutoCommit(false);

			mysqlDataSource = new HikariMysqlDataSource(connection, new ObjectMapperFactory().objectMapperFactory());
        }
        return mysqlDataSource;
    }

    public void close() throws Exception {
        //closing datasource
        if(mysqlDataSource != null) {
            mysqlDataSource.close();
        }
        closeContainer();
    }

    @SneakyThrows
    public void loadSchema(final List<String> statements) {
        Class.forName("com.mysql.jdbc.Driver");
        try (Connection connection = getMysqlDataSource().getDataSource().getConnection()) {
            try {
                for (String statement : statements) {
                	if(!statement.endsWith(";"))
					{
						statement = statement.concat(";");
					}
                    connection.prepareStatement(statement).execute();
                }
                connection.commit();
            } catch (Exception ex) {
                connection.rollback();
                throw ex;
            }
        } catch (Exception cause) {

            throw new EtcsoftException(DATABASE_SCHEMA_ERROR,
									   format("Error loading schema, exception: %s!!!", cause.getMessage()),
									   cause);
        }
    }

	@SneakyThrows
	public void loadSchema(final String sql) {
		loadSchema(Arrays.asList(sql.split(";")));
	}
}
