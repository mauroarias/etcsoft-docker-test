package org.etcsoft.dockertest.docker;

import org.etcsoft.dockertest.docker.DockerContainer;
import org.etcsoft.mysql.connector.repository.MysqlDataSource;

import java.util.List;

public interface MysqlDocker extends DockerContainer {
	MysqlDataSource getMysqlDataSource();
    void loadSchema(List<String> statements);
	void loadSchema(String statement);
}
