package org.etcsoft.dockertest.docker;

import java.util.Map;

public interface DockerContainer {
    void close() throws Exception;
    String getImageName();
    String getContainerId();
    boolean isClosed();
    Map<String, DockerContainer> getParentContainers();
}
