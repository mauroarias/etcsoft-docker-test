package org.etcsoft.dockertest.docker;

import com.spotify.docker.client.DefaultDockerClient;
import com.spotify.docker.client.DockerClient;
import lombok.SneakyThrows;
import org.etcsoft.elasticsearch.connector.config.EsJestConfig;
import org.etcsoft.elasticsearch.connector.factory.JestClientProvider;
import org.etcsoft.elasticsearch.connector.repository.EsClient;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.etcsoft.dockertest.DockerConstants.DOCKER_ES_HOST;
import static org.etcsoft.dockertest.DockerConstants.DOCKER_ES_HTTP_SCHEMA;
import static org.etcsoft.dockertest.DockerConstants.DOCKER_ES_WAITING_LOOP;
import static org.etcsoft.dockertest.DockerConstants.DOCKER_ES_WAITING_START_MESSAGE;
import static org.etcsoft.dockertest.DockerConstants.DOCKER_URI;

public class EsDefaultDocker extends DockerAbstractContainer implements EsDocker  {
    private final static String esPort = "9200";
    private EsClient EsClient = null;

    public EsDefaultDocker(String imageName,
                           String imageTag,
                           String hostname,
                           boolean autoPull,
                           Set<String> containerLinks,
                           Map<String, DockerContainer> parentContainers) {
        this(imageName,
             imageTag,
             hostname,
             null,
             null,
             autoPull,
             DOCKER_ES_WAITING_LOOP,
             containerLinks,
             parentContainers);
    }

    EsDefaultDocker(String imageName,
                    String imageTag,
                    String hostname,
                    DockerClient dockerClient,
                    EsClient EsClient,
                    boolean autoPull,
                    int secWaitingLoop,
                    Set<String> containerLinks,
                    Map<String, DockerContainer> parentContainers) {
        super(imageName,
              imageTag,
              hostname,
              DOCKER_ES_WAITING_START_MESSAGE,
              secWaitingLoop,
              autoPull,
              dockerClient == null ? new DefaultDockerClient(DOCKER_URI) : dockerClient,
              new HashSet<>(Arrays.asList("http.host=0.0.0.0", "transport.host=127.0.0.1")),
              new HashSet<String>() {{ add(String.valueOf(esPort)); }},
              containerLinks,
              parentContainers
             );
        this.EsClient = EsClient != null ? EsClient : getEsClient();
    }

    @Override
    @SneakyThrows
    public EsClient getEsClient() {
        if(EsClient == null) {
            EsJestConfig esJestConfig =
                    new EsJestConfig(DOCKER_ES_HOST,
                                     Integer.valueOf(getRandomPort(esPort)));
            this.EsClient = new JestClientProvider(esJestConfig, true).getClient();
        }
        return this.EsClient;
    }

    @Override
    public void close() throws Exception {
        if(EsClient != null) {
            EsClient.close();
        }
        closeContainer();
    }
}
