package org.etcsoft.dockertest.docker;

import  org.etcsoft.elasticsearch.connector.repository.EsClient;

public interface EsDocker extends DockerContainer {
    EsClient getEsClient();
}
