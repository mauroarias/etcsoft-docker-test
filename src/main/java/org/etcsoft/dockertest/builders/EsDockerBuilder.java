package org.etcsoft.dockertest.builders;

import org.etcsoft.dockertest.docker.DockerContainer;
import org.etcsoft.dockertest.docker.EsDefaultDocker;
import org.etcsoft.dockertest.docker.EsDocker;
import org.etcsoft.tools.validation.Validator;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static java.lang.String.format;
import static org.etcsoft.dockertest.DockerConstants.DOCKER_ES_IMAGE;
import static org.etcsoft.dockertest.DockerConstants.DOCKER_ES_IMAGE_TAG;
import static org.etcsoft.tools.exception.UtilsErrorCode.INPUT_ERROR;

public final class EsDockerBuilder {
	private final Validator validation = new Validator();
	private final Set<String> containerLinks = new HashSet<>();
	private final Map<String, DockerContainer> parentContainers = new HashMap<>();
    private String elasticSearchImageName = DOCKER_ES_IMAGE;
    private String elasticSearchImageTag = DOCKER_ES_IMAGE_TAG;
    private String hostname;
    private boolean autoPull = false;

	EsDockerBuilder() {}

    public EsDockerBuilder withMysqlImageName(final String mysqlImageName) {
        this.elasticSearchImageName = mysqlImageName;
        return this;
    }

    public EsDockerBuilder withMysqlImageTag(final String mysqlImageTag) {
        this.elasticSearchImageTag = mysqlImageTag;
        return this;
    }

    public EsDockerBuilder withHostname(final String hostname) {
        this.hostname = hostname;
        return this;
    }

    public EsDockerBuilder withAutoPull(final boolean autoPull) {
        this.autoPull = autoPull;
        return this;
    }

    public EsDockerBuilder addContainerLink(final String containerId, final String alias) {
        validation.throwIfInstanceBlank(containerId, "containerId cannot be blank", INPUT_ERROR);
		validation.throwIfInstanceBlank(alias, "alias cannot be blank", INPUT_ERROR);
        this.containerLinks.add(format("%s:%s", containerId, alias));
        return this;
    }

    public EsDockerBuilder addParentContainer(final String name, final DockerContainer container) {
        this.parentContainers.put(name, container);
        return this;
    }

    public EsDocker build() {
        return new EsDefaultDocker(elasticSearchImageName,
                                   elasticSearchImageTag,
                                   hostname,
                                   autoPull,
                                   containerLinks,
                                   parentContainers);
    }
}
