package org.etcsoft.dockertest.builders;

import org.etcsoft.dockertest.docker.DockerContainer;
import org.etcsoft.dockertest.docker.MysqlDefaultDocker;
import org.etcsoft.dockertest.docker.MysqlDocker;
import org.etcsoft.tools.validation.Validator;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static java.lang.String.format;
import static org.etcsoft.dockertest.DockerConstants.DOCKER_MYSQL_IMAGE;
import static org.etcsoft.dockertest.DockerConstants.DOCKER_MYSQL_IMAGE_TAG;
import static org.etcsoft.tools.exception.UtilsErrorCode.INPUT_ERROR;

public final class MysqlDBDockerBuilder {
	private final Validator validation = new Validator();
	private final Set<String> containerLinks = new HashSet<>();
	private final Map<String, DockerContainer> parentContainers = new HashMap<>();
    private String mysqlImageName = DOCKER_MYSQL_IMAGE;
    private String mysqlImageTag = DOCKER_MYSQL_IMAGE_TAG;
    private String hostname;
    private boolean autoPull = false;

	MysqlDBDockerBuilder() {}

    public MysqlDBDockerBuilder withMysqlImageName(final String mysqlImageName) {
        this.mysqlImageName = mysqlImageName;
        return this;
    }

    public MysqlDBDockerBuilder withMysqlImageTag(final String mysqlImageTag) {
        this.mysqlImageTag = mysqlImageTag;
        return this;
    }

    public MysqlDBDockerBuilder withHostname(final String hostname) {
        this.hostname = hostname;
        return this;
    }

    public MysqlDBDockerBuilder withAutoPull(final boolean autoPull) {
        this.autoPull = autoPull;
        return this;
    }

    public MysqlDBDockerBuilder addContainerLink(final String containerId, final String alias) {
        validation.throwIfInstanceBlank(containerId, "containerId cannot be blank", INPUT_ERROR);
		validation.throwIfInstanceBlank(alias, "alias cannot be blank", INPUT_ERROR);
        this.containerLinks.add(format("%s:%s", containerId, alias));
        return this;
    }

    public MysqlDBDockerBuilder addParentContainer(final String name, final DockerContainer container) {
        this.parentContainers.put(name, container);
        return this;
    }

    public MysqlDocker build() {
        return new MysqlDefaultDocker(mysqlImageName,
									  mysqlImageTag,
									  hostname,
									  autoPull,
									  containerLinks,
									  parentContainers);
    }
}
