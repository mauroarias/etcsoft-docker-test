package org.etcsoft.dockertest.builders;

public class DockerContainerFactory {

    public static MysqlDBDockerBuilder getMysqlBuilder() {
        return new MysqlDBDockerBuilder();
    }

    public static EsDockerBuilder getEsBuilder() {
        return new EsDockerBuilder();
    }
}
