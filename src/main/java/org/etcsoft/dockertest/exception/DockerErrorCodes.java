package org.etcsoft.dockertest.exception;

import lombok.Getter;
import org.etcsoft.tools.exception.ErrorCode;
import org.etcsoft.tools.model.HttpStatus;

import static org.etcsoft.tools.model.HttpStatus.INTERNAL_SERVER_ERROR;

public enum DockerErrorCodes implements ErrorCode {
    DOCKER_CLIENT_ERROR(INTERNAL_SERVER_ERROR),
    DOCKER_STARTING_TIMEOUT(INTERNAL_SERVER_ERROR),
    DOCKER_INTERRUPTED(INTERNAL_SERVER_ERROR),
    DOCKER_STARTING_ERROR(INTERNAL_SERVER_ERROR),
    WRONG_CONTAINER_TYPE(INTERNAL_SERVER_ERROR);

	@Getter
	private final HttpStatus httpStatus;

	DockerErrorCodes(HttpStatus httpStatus) {
			this.httpStatus = httpStatus;
		}
}
