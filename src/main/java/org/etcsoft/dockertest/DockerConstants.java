package org.etcsoft.dockertest;

public final class DockerConstants {
    public final static String DOCKER_URI = "unix:///var/run/docker.sock";
    public final static int DOCKER_WAITING_LOOP_DURATION = 1000; //in miliseconds

    //mysql default docker image and config
    public final static String DOCKER_MYSQL_IMAGE = "mauroarias/mysql-oraclelinux7";
    public final static String DOCKER_MYSQL_IMAGE_TAG = "5.6.34";
    public final static String DOCKER_MYSQL_WAITING_START_MESSAGE = "MySQL init process done. Ready for start up";
    public final static String DOCKER_MYSQL_USER = "root";
    public final static String DOCKER_MYSQL_PASSWD = "admin";
    public final static String DOCKER_MYSQL_HOST = "localhost";
    public final static String DOCKER_MYSQL_PORT = "3306";
    public final static int DOCKER_MYSQL_WAITING_LOOP = 60;

    //es default docker image and config
    public final static String DOCKER_ES_IMAGE = "elasticsearch";
    public final static String DOCKER_ES_IMAGE_TAG = "2.4";
    public final static String DOCKER_ES_WAITING_START_MESSAGE = "indices into cluster_state";
    public final static String DOCKER_ES_HTTP_SCHEMA = "http";
    public final static String DOCKER_ES_HOST = "localhost";
    public final static int DOCKER_ES_WAITING_LOOP = 60;

    //spring defaul docker config
    public final static String DOCKER_SPRING_STARTING_FAIL = "Application startup failed";
}
