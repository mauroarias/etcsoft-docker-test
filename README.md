# Docker Test

[![Maven Central](https://maven-badges.herokuapp.com/maven-central/org.etcsoft/etcsoft-docker-test/badge.svg)](https://maven-badges.herokuapp.com/maven-central/org.etcsoft/etcsoft-docker-test)
[![Build Status](https://travis-ci.org/mauroarias/etcsoft-docker-test.svg?branch=master)](https://travis-ci.org/mauroarias/etcsoft-docker-test)

This library allows to handle easily test containers used inside of integration tests.

## Requirements
* java 8
* Maven 3
* spotify - docker-maven-plugin
 
## COMPONENTS SUPPORTED
* Mysql.

## TESTS

### Unit test
This project includes unit tests of the some business class, using Junit and mockito frameworks.
please see ../src/test/java/org/etcsoft/dockertest.docker to find more details

## Compiling & Running tests  

### running unit test
```
mvn clean install
```

### skipping unit tests
```
mvn clean install -DskipTests
```

### running integration tests
```
mvn clean install -DskipTestsIt=false
```

## HOW TO USE

### Mysql instances:

use **MysqlDBDockerBuilder** to build a **MysqlDocker** instance as:

```
DockerContainerFactory.getMysqlBuilder()
        .mysqlImageName(mysqlImageName)         //mandatory image name. Example: "mauroarias/mysql-oraclelinux7"
        .mysqlImageTag(mysqlImageTag)           //optional tag, by default "latest". Example: "5.6.34"
        .hostname(hostname)                     //optional hostname, by default empty. Example "myhostname"
        .autoPull(true)                         //optional flag to auto pull images, by default false
        .addContainerLink(containerId, alias)   //optional containers links, by default empty. These are container 
                                                //  will be linked to this container
        .addParentContainer(container)          //optional containers linked to be closed when the container is closed
        .build()                                //create and start the container Mysql.
```
 
